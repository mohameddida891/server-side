export declare class CreateTransactionDto {
    readonly amount: number;
    readonly category: string;
    readonly date: string;
    readonly description?: string;
}
