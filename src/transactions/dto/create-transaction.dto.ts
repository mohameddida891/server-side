/* eslint-disable prettier/prettier */
import {
    IsNotEmpty,
    IsNumber,
    IsOptional,
    IsString
} from 'class-validator';

export class CreateTransactionDto {
    @IsNotEmpty()
    @IsNumber()
    readonly amount: number;

    @IsNotEmpty()
    @IsString()
    readonly category: string;

    @IsNotEmpty()
    @IsString()
    readonly date: string;

    @IsOptional()
    @IsString()
    readonly description?: string;
}
