/* eslint-disable prettier/prettier */
import { Prop } from '@nestjs/mongoose';

export class UpdateUserDto {
    @Prop()
    password: string;

    @Prop()
    email: string;
}
