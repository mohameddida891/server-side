/* eslint-disable prettier/prettier */
import { Prop } from "@nestjs/mongoose";

export class CreateUserDto {
    @Prop()
    password: string;

    @Prop()
    email: string;
}
